For this application I used couple of little helps that enhanced my developing time.
I didn’t got much time to develop a solution as my best, but I used all my spare time to build something acceptable for a test.

What about the Design Pattern, I choose a kind of standard MCV strong divided between sections.
For the UI, if no one gave me an example and leave to me all the choice about UX+UI, I use to visit pttrns.com to inspire myself about some “topic APPS”.

With the topic “travel and flights” I decided to replace the UX like Passbook (now Apple Wallet).
## What about the helps discussed above, here is the list
+ CocoaPod 
    * MBProgressHUD for a “standard  thinking-UI Object”
    * Overcoat
        1. Mantle for JSON Mapping
        2. AFNetworking for Network Stack
    * SDWebImage -> Image Download + Caching (not used for lessTime)

Last but not least, versioning is designed to replace a typical commitment by a Client.
The latest  commits are directly on "develop" due to simulate the "impending" deadline and fast changes with the eye oriented to a "fast-prototyping" project.

In addition, the provided JSON from #pastebin fill the response as Content-Type plain/text that could make difficult to fill the JSON in proper way in terms of parsing a plainText in NSDictionary + NSArray and doesn't work with my provided Library that handle the parsing for me. I moved the JSONs into a free service that's response with the standard Content-Type. 

NB: make a **pod install** on the Project's Root first and open .xcworkspace instead of .xcodeproj then. 

###### Enjoy, Dario